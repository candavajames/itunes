//
//  UIView+.swift
//  ItunesSearch
//
//  Created by Jameson Candava on 05/11/2019.
//  Copyright © 2019 Jameson Candava. All rights reserved.
//

import UIKit

extension UIView {
    
    func makeCornersRounded() {
         self.layer.cornerRadius = 8
         self.clipsToBounds = true
     }
    
    func addFullShadow() {
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 3, height: 3)
        self.layer.shadowOpacity = 0.20
        self.layer.shadowRadius = 3.0
        self.layer.masksToBounds = false
    }
}
