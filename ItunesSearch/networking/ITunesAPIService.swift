//
//  ITunesAPIService.swift
//  ItunesSearch
//
//  Created by Jameson Candava on 03/11/2019.
//  Copyright © 2019 Jameson Candava. All rights reserved.
//

import Moya

public class ItunesApiServiceProvider: MoyaProvider<ITunesApiService> {
    public init() {
        
    }
}

// MARK: - API Service for Itunes
public enum ITunesApiService {
    case search(request: SearchRequest)
}

extension ITunesApiService: TargetType {
    
    var base: String { return "https://itunes.apple.com/" }
    public var baseURL: URL {
        return URL(string: base)!
    }
    
    public var path: String {
        switch self {
        case .search:
            return "search"
        }
    }
    
    public var method: Moya.Method {
        switch self {
        case .search:
            return .get
        }
    }
    
    public var sampleData: Data {
        return Data()
    }
    
    public var task: Task {
        switch self {
        case .search(let request):
            return .requestParameters(parameters: request.asDictionary(), encoding: URLEncoding.default)
        }
    }
    
    public var headers: [String : String]? {
        return ["Content-Type": "application/json"]
    }
    
}

extension Encodable {
    
    func asDictionary() -> [String: Any] {
        do {
            let data = try JSONEncoder().encode(self)
            guard let dictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] else {
                return [:]
            }
            return dictionary
        } catch {
            return [:]
        }
    }
}
