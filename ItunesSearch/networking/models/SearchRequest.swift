//
//  SearchRequest.swift
//  ItunesSearch
//
//  Created by Jameson Candava on 04/11/2019.
//  Copyright © 2019 Jameson Candava. All rights reserved.
//

import Foundation

public struct SearchRequest: Codable {
    
    public var term: String
    public var country: String
    public var media: String
    
    enum CodingKeys: String, CodingKey {
        case term = "term"
        case country = "country"
        case media = "media"
    }
}
