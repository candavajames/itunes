//
//  Search.swift
//  ItunesSearch
//
//  Created by Jameson Candava on 03/11/2019.
//  Copyright © 2019 Jameson Candava. All rights reserved.
//

import Foundation

public struct Search: Codable {
    
    public var resultCount: Int
    public var searchResult: [SearchResult]
    
    enum CodingKeys: String, CodingKey {
        case resultCount = "resultCount"
        case searchResult = "results"
    }
    
}
