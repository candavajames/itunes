//
//  SearchResult.swift
//  ItunesSearch
//
//  Created by Jameson Candava on 03/11/2019.
//  Copyright © 2019 Jameson Candava. All rights reserved.
//

import Foundation

public struct SearchResult: Codable {
    
    public let artistName: String
    public let artworkUrl100: String
    public let primaryGenreName: String
    public let collectionPrice: Double?
    public let longDescription: String
    
    enum CodingKeys: String, CodingKey {
        case artistName = "artistName"
        case artworkUrl100 = "artworkUrl100"
        case primaryGenreName = "primaryGenreName"
        case collectionPrice = "collectionPrice"
        case longDescription = "longDescription"
    
    }
    
    
}
