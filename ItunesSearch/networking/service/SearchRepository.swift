//
//  SearchRepository.swift
//  ItunesSearch
//
//  Created by Jameson Candava on 03/11/2019.
//  Copyright © 2019 Jameson Candava. All rights reserved.
//

import RxSwift
import Moya

public class SearchRepository {
    
    private let itunesStorage: ItunesStorage
    private let service: MoyaProvider<ITunesApiService>
    
    init(itunesStorage: ItunesStorage,
         service: MoyaProvider<ITunesApiService>) {
        self.itunesStorage = itunesStorage
        self.service = service
    }
    
    func search() -> Observable<Search>  {
        
        let request = SearchRequest(term: "star", country: "au", media: "movie")

        if let search = itunesStorage.searchResult, search.resultCount > 0 {
            return Observable.deferred({
                return Observable.just(search)
            }).asObservable()
        }

        return service.rx.request(.search(request: request))
            .map(Search.self)
            .do(onSuccess: { result in
                // Save to keychain
                self.itunesStorage.searchResult = result
                self.itunesStorage.dateResult = Date()
            })
            .asObservable()
    }
    
    func getSearchDate() -> Observable<Date> {
        
        if let searchDate = itunesStorage.dateResult {
            return Observable.deferred({
                return Observable.just(searchDate)
            }).asObservable()
        }
        
        return Observable.empty()

    }
}

