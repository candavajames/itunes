//
//  ITunesStorage.swift
//  ItunesSearch
//
//  Created by Jameson Candava on 03/11/2019.
//  Copyright © 2019 Jameson Candava. All rights reserved.
//

import SwiftKeychainWrapper

public class ItunesStorage {
    
    private let searchResultKey = "searchResultsKey"
    private let searchDateKey = "searchDateKey"
    
    public var searchResult: Search? {
        get {
            if let searchResultString = KeychainWrapper.standard.string(forKey: searchResultKey) {
                let jsonDecoder = JSONDecoder()
                let jsonData = searchResultString.data(using: .utf8)!
                if let searchResult = try? jsonDecoder.decode(Search.self, from: jsonData) {
                    return searchResult
                } else {
                    KeychainWrapper.standard.removeObject(forKey: searchResultKey)
                    return nil
                }
            }
            return nil
        }
        set {
            if let data = newValue {
                let jsonEncoder = JSONEncoder()
                let jsonData = try! jsonEncoder.encode(data)
                let jsonString = String(data: jsonData, encoding: .utf8)
                KeychainWrapper.standard.set(jsonString!, forKey: searchResultKey)
            } else {
                KeychainWrapper.standard.removeObject(forKey: searchResultKey)
            }
        }
    }
    
    public var dateResult: Date? {
        get {
            if let searchDate = KeychainWrapper.standard.string(forKey: searchDateKey) {
                let jsonDecoder = JSONDecoder()
                let jsonData = searchDate.data(using: .utf8)!
                if let date = try? jsonDecoder.decode(Date.self, from: jsonData) {
                    return date
                } else {
                    KeychainWrapper.standard.removeObject(forKey: searchDateKey)
                    return nil
                }
            }
            return nil
        }
        set {
            if let data = newValue {
                let jsonEncoder = JSONEncoder()
                let jsonData = try! jsonEncoder.encode(data)
                let jsonString = String(data: jsonData, encoding: .utf8)
                KeychainWrapper.standard.set(jsonString!, forKey: searchDateKey)
            } else {
                KeychainWrapper.standard.removeObject(forKey: searchDateKey)
            }
        }
    }
}
