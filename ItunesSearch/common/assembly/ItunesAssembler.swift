//
//  ItunesAssembler.swift
//  ItunesSearch
//
//  Created by Jameson Candava on 03/11/2019.
//  Copyright © 2019 Jameson Candava. All rights reserved.
//

import Swinject

public class ItunesAssembler {
    
    public static let instance = ItunesAssembler()
    
    public private(set) var resolver: Resolver!
    
    private init() {
        let assembler = Assembler(
            [Assembly](), container: Container()
        )
        self.resolver = assembler.resolver
        
        assembler.apply(assembly: ItunesDataAssembly())
        assembler.apply(assembly: ItunesPresentationAssembly())
    }
}
