//
//  ItunesPresentationAssembly.swift
//  ItunesSearch
//
//  Created by Jameson Candava on 03/11/2019.
//  Copyright © 2019 Jameson Candava. All rights reserved.
//

import Swinject
import SwinjectAutoregistration

public class ItunesPresentationAssembly: Assembly {
    
    public func assemble(container: Container) {
        container.autoregister(SearchViewModel.self, initializer: SearchViewModel.init)
    }
}
