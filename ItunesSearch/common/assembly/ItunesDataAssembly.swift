//
//  ItunesDataAssembly.swift
//  ItunesSearch
//
//  Created by Jameson Candava on 03/11/2019.
//  Copyright © 2019 Jameson Candava. All rights reserved.
//

import Swinject
import SwinjectAutoregistration
import Moya

public class ItunesDataAssembly: Assembly {

    public func assemble(container: Container) {
        container.autoregister(ItunesStorage.self, initializer: ItunesStorage.init).inObjectScope(.container)
        
        container.register(MoyaProvider<ITunesApiService>.self) { r in
            return MoyaProvider<ITunesApiService>(plugins: [NetworkLoggerPlugin(verbose: true)])
        }.inObjectScope(.container)
        
        // MARK: - Repositories
        container.autoregister(SearchRepository.self, initializer: SearchRepository.init).inObjectScope(.container)
    }
    
}

