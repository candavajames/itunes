//
//  SearchCollectionViewCell.swift
//  ItunesSearch
//
//  Created by Jameson Candava on 03/11/2019.
//  Copyright © 2019 Jameson Candava. All rights reserved.
//

import UIKit
import SnapKit

class SearchCollectionViewCell: UICollectionViewCell {
    
    lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = #colorLiteral(red: 0.9647058824, green: 0.9764705882, blue: 1, alpha: 1)
        view.makeCornersRounded()
        view.addFullShadow()
        return view
    }()
    
    lazy var artworkImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.makeRounded()
        return imageView
    }()
    
    lazy var trackName: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        return label
    }()

    
    lazy var genreLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        return label
    }()
    
    
    lazy var priceLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 8)
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupCell()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupCell() {
        
       
        addSubview(containerView)
        containerView.addSubview(artworkImageView)
        containerView.addSubview(trackName)
        containerView.addSubview(genreLabel)
        containerView.addSubview(priceLabel)
        
        containerView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        artworkImageView.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(16)
            make.top.equalToSuperview().offset(8)
            make.height.width.equalTo(80)
        }
        
        trackName.snp.makeConstraints { (make) in
            make.leading.equalTo(artworkImageView.snp.trailing).offset(16)
            make.trailing.equalTo(-24)
            make.top.equalTo(artworkImageView.snp.top).offset(16)
        }
        
        genreLabel.snp.makeConstraints { (make) in
            make.top.equalTo(trackName.snp.bottom)
            make.leading.trailing.equalTo(trackName)
        }
        
        priceLabel.snp.makeConstraints { (make) in
            make.top.equalTo(genreLabel.snp.bottom)
            make.leading.trailing.equalTo(trackName)
        }
        
    }
    
    
}
