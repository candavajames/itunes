//
//  SearchViewController.swift
//  ItunesSearch
//
//  Created by Jameson Candava on 03/11/2019.
//  Copyright © 2019 Jameson Candava. All rights reserved.
//

import UIKit
import RxSwift
import SnapKit
import Kingfisher

// MARK: - Setup initial value
extension SearchViewController {
    
    func setup(searchViewModel: SearchViewModel) {
        self.searchViewModel = searchViewModel
    }
}

class SearchViewController: UIViewController {
    
    // MARK: - Interface Builder
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.itemSize = CGSize(width: 340, height: 100)
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .clear
        return collectionView
    }()
    
    // MARK: - Properties
    var searchViewModel: SearchViewModel?
    let disposeBag = DisposeBag()
    
}

// MARK: - Main
extension SearchViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        searchViewModel?.get()
        searchViewModel?.getSearchDate()
    }
    
    func setupUI() {
        setupSubviews()
        setupAutoLayout()
        setupCollectionView()
        setupBindings()
    }

}


