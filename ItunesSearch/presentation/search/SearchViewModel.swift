//
//  SearchViewModel.swift
//  ItunesSearch
//
//  Created by Jameson Candava on 03/11/2019.
//  Copyright © 2019 Jameson Candava. All rights reserved.
//

import RxCocoa
import RxSwift


public class SearchViewModel {
    
    private let searchRepository: SearchRepository
    private let disposeBag: DisposeBag
    
    public private(set) var searchResults: BehaviorRelay<[SearchResult]>
    public private(set) var searchDate: BehaviorRelay<Date>
    
    public init(searchRepository: SearchRepository) {
        self.searchRepository = searchRepository
        self.searchResults = BehaviorRelay<[SearchResult]>(value: [])
        self.searchDate = BehaviorRelay<Date>(value: Date())
        self.disposeBag = DisposeBag()
    }
    
    func get() {
        self.searchRepository.search().subscribe(onNext: { result in
            self.searchResults.accept(result.searchResult)
        }).disposed(by: disposeBag)
    }
    
    func getSearchDate() {
        self.searchRepository.getSearchDate().subscribe(onNext: { date in
            self.searchDate.accept(date)
        }).disposed(by: disposeBag)
    }
    
}
