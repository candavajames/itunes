//
//  SearchViewController+UI.swift
//  ItunesSearch
//
//  Created by Jameson Candava on 04/11/2019.
//  Copyright © 2019 Jameson Candava. All rights reserved.
//

import UIKit

extension SearchViewController {
    
    // MARK: - Setup collection view 
    public func setupCollectionView() {
        collectionView.register(SearchCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        
        collectionView.contentInset = UIEdgeInsets(
            top: CGFloat(24),
            left: 0,
            bottom: CGFloat(24),
            right: 0
        )
    }
    
    // MARK: - Add collection view
    public func setupSubviews() {
        view.addSubview(collectionView)
    }
    
    // MARK: - Setup constraint for views.
    public func setupAutoLayout() {
        
        collectionView.snp.makeConstraints { (make) in
            make.top.equalTo(24)
            make.bottom.leading.trailing.equalToSuperview()
        }
    }
    
    // MARK: - Bindings to view model
    public func setupBindings() {
        
        // MARK: - Bind date from view model
        self.searchViewModel?.searchResults.bind(to: collectionView.rx.items(cellIdentifier: "cell", cellType: SearchCollectionViewCell.self)) { row, model, cell in
            cell.trackName.text = model.artistName
            cell.artworkImageView.kf.setImage(with: URL(string: model.artworkUrl100), placeholder: UIImage(named: "placeholder"))
            cell.genreLabel.text = model.primaryGenreName
            cell.priceLabel.text = String(format: "%.2f", model.collectionPrice ?? 0)
            
        }.disposed(by: disposeBag)
        
        
        // MARK: - Delegate to select item from collection view
        collectionView.rx.modelSelected(SearchResult.self)
            .subscribe(onNext: { searchResult in
                
                let searchDetailVC = SearchDetailViewController()
                searchDetailVC.setup(searchResult: searchResult)
                self.navigationController?.pushViewController(searchDetailVC, animated: true)
                
            }).disposed(by: disposeBag)
        
        // MARK: - Subscribe for date search
        searchViewModel?.searchDate.subscribe(onNext: { date in
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .medium
            dateFormatter.timeStyle = .none
            dateFormatter.locale = Locale(identifier: "en_US")
            
            self.navigationItem.title = "ITunes Search - \(dateFormatter.string(from:date))"
        }).disposed(by: disposeBag)
    }
}
