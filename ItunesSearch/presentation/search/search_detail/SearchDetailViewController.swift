//
//  SearchDetailViewController.swift
//  ItunesSearch
//
//  Created by Jameson Candava on 05/11/2019.
//  Copyright © 2019 Jameson Candava. All rights reserved.
//

import UIKit
import RxSwift
import SnapKit


// MARK: - Setup initial value search result
extension SearchDetailViewController {
    
    func setup(searchResult: SearchResult) {
        self.searchResult = searchResult
    }
    
}

class SearchDetailViewController: UIViewController {
    
    //MARK: - Properties
    var searchResult: SearchResult?
    
    // MARK: - User Interface
    lazy var scrollView: UIScrollView = {
        let view = UIScrollView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var contentView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var artworkImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.frame =  CGRect(x: 0, y: 0, width: 300, height: 100)
        imageView.contentMode = .center
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    lazy var trackName: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        label.textAlignment = .left
        return label
    }()
    
    
    lazy var genreLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.textAlignment = .left
        return label
    }()
    
    
    lazy var priceLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 8)
        label.textAlignment = .left
        return label
    }()
    
    
    lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.textAlignment = .left
        label.numberOfLines = 0
        return label
    }()
    
    
}

// MARK: - Design
extension SearchDetailViewController {
    
    func setupSubviews() {
        
        view.backgroundColor = #colorLiteral(red: 0.9647058824, green: 0.9764705882, blue: 1, alpha: 1)
        
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        
        contentView.addSubview(artworkImageView)
        contentView.addSubview(trackName)
        contentView.addSubview(genreLabel)
        contentView.addSubview(priceLabel)
        contentView.addSubview(descriptionLabel)
        
    }
}


// MARK: - AutoLayout
extension SearchDetailViewController {
    
    func setupAutoLayout() {
        
        scrollView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        contentView.snp.makeConstraints { (make) in
            make.top.leading.trailing.bottom.equalTo(scrollView)
            make.height.equalTo(scrollView).priority(.low)
            make.width.equalTo(scrollView)
        }
        
        artworkImageView.snp.makeConstraints { (make) in
            make.top.equalTo(24)
            make.leading.trailing.equalToSuperview()
            make.centerX.equalToSuperview()
            make.height.equalTo(100)
        }
        
        trackName.snp.makeConstraints { (make) in
            make.top.equalTo(artworkImageView.snp.bottom).offset(16)
            make.leading.equalTo(16)
            make.trailing.equalTo(-16)
        }
        
        genreLabel.snp.makeConstraints { (make) in
            make.top.equalTo(trackName.snp.bottom).offset(8)
            make.leading.equalTo(16)
            make.trailing.equalTo(-16)
        }
        
        priceLabel.snp.makeConstraints { (make) in
            make.top.equalTo(genreLabel.snp.bottom).offset(8)
            make.leading.equalTo(16)
            make.trailing.equalTo(-16)
        }
        
        descriptionLabel.snp.makeConstraints { (make) in
            make.top.equalTo(priceLabel.snp.bottom).offset(8)
            make.leading.equalTo(16)
            make.trailing.equalTo(-16)
            make.bottom.equalTo(16)
        }
        
    }
}
// MARK: - Main
extension SearchDetailViewController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI() {
        setupSubviews()
        setupAutoLayout()
        setupContent()
    }
    
    
    private func setupContent() {
        
        guard let searchResult = self.searchResult else {
            return
        }
        
        navigationController?.title = searchResult.artistName
        
        artworkImageView.kf.setImage(with: URL(string: searchResult.artworkUrl100), placeholder: UIImage(named: "placeholder"))
        trackName.text = searchResult.artistName
        genreLabel.text = "GENRE: \(searchResult.primaryGenreName)"
        priceLabel.text = "PRICE: \(String(format: "%.2f", searchResult.collectionPrice ?? 0))"
        
        descriptionLabel.text = searchResult.longDescription
        
    }
    
}
